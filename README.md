# Tplus Plugins

This repo defines all plugins available in Tplus, Tplus-Server can start anything in this repo that has an entry in `index.json`
and a corresponding `services.yml(.twig)` file. The `services.yml(.twig)` files are meant to be similar to a docker-compose file, but need a bit
more information for Tplus to handle things like exposure of Endpoints, volume backups, and represet Configuration in the UI in a user-friendly way.


Currently supports:
* TezosLive Websocket
* TezosLive GraphQL
* Better Call Dev 
* TzStats Indexer / Frontend

### Documentation:

Current documentation for Tplus is available at [tplus.dev](https://tplus.dev)

### Join our Community

For feedback and questions, please find us @

* [Telegram](https://t.me/tuliptools)
* [Twitter](https://twitter.com/TulipToolsOU)
* [tezos-dev Slack](https://tezos-dev.slack.com/#/)

### Related Repositories:
* [Tplus Main Repo](https://gitlab.com/tuliptools/tplus)
* [Demo Projects](https://gitlab.com/tuliptools/tplusdemoprojects)
* [User Interface](https://gitlab.com/tuliptools/tplusgui)
* [CLI Tool](https://gitlab.com/tuliptools/TplusCLI)

Tplus is developed by [TulipTools](https://tulip.tools/)

![Tulip Logo](https://tulip.tools/wp-content/uploads/2020/06/tulip_small-2.png)
